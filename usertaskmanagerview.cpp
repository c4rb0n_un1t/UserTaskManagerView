#include "usertaskmanagerview.h"
#include "ui_form.h"

UserTaskManagerView::UserTaskManagerView() :
	QObject(nullptr),
	PluginBase(this),
	ui(new Ui::Form),
	m_GUIElementBase(new GUIElementBase(this, {"MainMenuItem"}))
{
	ui->setupUi(m_GUIElementBase);
	taskTree = nullptr;
	proxyModel = nullptr;

	connect(ui->buttonExit, &QPushButton::clicked, this, &UserTaskManagerView::buttonExit_clicked);

	connect(ui->myTreeView, &QTreeView::entered, this, &UserTaskManagerView::onTreeViewClicked);
	connect(ui->myTreeView, &QTreeView::activated, this, &UserTaskManagerView::onTreeViewClicked);
	connect(ui->myTreeView, &QTreeView::clicked, this, &UserTaskManagerView::onTreeViewClicked);
	connect(ui->myTreeView, &QTreeView::pressed, this, &UserTaskManagerView::onTreeViewClicked);

	connect(ui->addForm, &AddForm::onAddedTask, this, [=](QModelIndex index) {
		ui->myTreeView->setExpanded(taskTreeFilter->mapFromSource(index), !ui->myTreeView->isExpanded(taskTreeFilter->mapFromSource(index)));
	});
	connect(ui->addForm, &AddForm::OnDelete, this, [=]() {
		auto indices = ui->myTreeView->selectionModel()->selectedRows();
		auto index = indices.empty() ? QModelIndex() : taskTreeFilter->mapToSource(indices.first());
		ui->addForm->ShowModelData(index);
	});

#ifdef Q_OS_ANDROID
	ui->buttonExit->setVisible(false);
#endif

	m_checkBoxItemDelegate = new CheckBoxItemDelegate(this);

	initPluginBase({
		{INTERFACE(IPlugin), this},
		{INTERFACE(IGUIElement), m_GUIElementBase}
	},
	{
		{INTERFACE(IUserTaskDataExtention), m_taskManager},
		{INTERFACE(IUserTaskDateDataExtention), m_dateTaskManager},
		{INTERFACE(IUserTaskRepeatDataExtention), m_repeatTaskManager},
		{INTERFACE(IUserTaskPomodoroDataExtention), m_pomodoroTaskManager},
	});
	m_GUIElementBase->initGUIElementBase();
}

void UserTaskManagerView::onReady()
{
	taskTree = m_taskManager->getModel();
	taskTreeFilter = taskTree->getFilter();
	taskTreeFilter->setColumns({
		{INTERFACE(IUserTaskDataExtention), {"name", "isDone"}}
	});
	model = new DesignProxyModel(taskTree);
	ui->addForm->SetModel(model);
	ui->myTreeView->setModel(taskTreeFilter);
}

void UserTaskManagerView::OnAddFormClosed()
{
	ui->buttonExit->setFocusPolicy(Qt::StrongFocus);
	ui->myTreeView->setFocusPolicy(Qt::StrongFocus);
	ui->myTreeView->setFocus();
}

void UserTaskManagerView::buttonExit_clicked()
{
	m_GUIElementBase->closeSelf();
}

void UserTaskManagerView::onTreeViewClicked(const QModelIndex &index)
{
	auto sourceIndex = taskTreeFilter->mapToSource(index);
	ui->addForm->ShowModelData(sourceIndex);
}
