#include "checkboxitemdelegate.h"

#include <QApplication>

CheckBoxItemDelegate::CheckBoxItemDelegate(QObject* parent) :
	QAbstractItemDelegate(parent)
{

}

void CheckBoxItemDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
	QStyleOptionButton checkBoxOption;
	checkBoxOption.features = QStyleOptionButton::ButtonFeature::None;
	checkBoxOption.icon = QIcon("qrc:/Res/ic_done_black_24dp.png");
	checkBoxOption.iconSize = QSize(50, 50);
	checkBoxOption.state = QStyle::State_Enabled;
	if (true)
		checkBoxOption.state |= QStyle::State_On;
	else
		checkBoxOption.state |= QStyle::State_Off;

	QApplication::style()->drawControl(QStyle::CE_CheckBox, &checkBoxOption, painter);
}

QSize CheckBoxItemDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
	return QSize(50, 50);
}
