#pragma once


#include <QDebug>
#include <QWidget>
#include <QDataWidgetMapper>
#include <QAbstractItemModel>
#include <QKeyEvent>
#include <QMap>
#include <QGridLayout>

#include "designproxymodel.h"

namespace Ui {
class AddForm;
}

class AddForm : public QWidget
{
	Q_OBJECT

public:
	explicit AddForm(QWidget *parent = nullptr);
	~AddForm();

	void SetModel(DesignProxyModel *model);
	void ShowModelData(const QModelIndex &index);

signals:
	void onAddedTask(QModelIndex index);
	void OnDelete();
	void OnClose();

public slots:
	void addTask();
	void deleteTask();

private:
	void ClearEditors();
	QWidget *GetStandardDataTypeEditor(
	        const Interface& interface, const QString& fieldName, const QVariant &value, QWidget *parent);
	QVariant GetDataFromStandardDataTypeEditor(
	        const Interface& interface, const QString& fieldName, const QVariant& value, QWidget* editor);
	void showModelDataPrivate(const ExtendableItemDataMap& header, const ExtendableItemDataMap& itemData);

private slots:
	void AcceptChanges();
	void CancelChanges();

	// QObject interface
public:
	bool event(QEvent *event) override;

private:
	struct EditWidgetData
	{
		QWidget* label;
		QWidget* editor;
		int type;
	};

	Ui::AddForm *ui;
	DesignProxyModel *model;
	QGridLayout *gridLayout;
	QModelIndex currentModelIndex;
	QMap<Interface, QMap<QString, EditWidgetData>> m_editWidgets;
};

